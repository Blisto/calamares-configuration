# ============================
- name: Installer Requirements
# ============================
  description: Required by the current installer
  hidden: false
  selected: true
  expanded: false
  critical: true
  immutable: true
  packages: 
    # - name: paru-bin
    #   description: For installing packages from the AUR
    - name: mkinitcpio
      description: A script to create the initial ramdisk. Required to re-run after configuring LVM, system encryption, or RAID
    - name: grub
      description: Bootloader required by the current installer version. Will be removed when bootloader is chosen and installed dynamically.
    - name: efibootmgr
      description: Required by the installer to setup the bootloader
    - name: sed
      description: Stream editor required to substitute entries in /etc/default/grub
    - name: os-prober
      description: For detecting other operating systems
    - name: dosfstools
      description: VFAT support for os-prober to mount Windows partitions
    - name: ntfs-3g
      description: NTFS support for os-prober to mount Windows partitions    

# ===================
- name: RebornOS Base
# ===================
  description: Essential packages. Modify at your own risk
  hidden: false
  selected: true
  expanded: false
  critical: true
  subgroups: 
    - name: Storage and Filesystems
      description: 
      subgroups: 
        - name: Filesystems
          description: Support for common filesystems
          subgroups: 
            - name: exFAT
              description: exFAT support
              packages: 
                - exfatprogs
                - exfat-utils
          packages:          
            - name: e2fsprogs
              description: ext3 and ext4 support
            - name: dosfstools
              description: VFAT support
    - name: Networking and Internet
      description:
      packages: 
        - networkmanager
    - name: Utilities
      description: Core Utilities

# =========
- name: Art
# =========
  description: Tools for your artistic pursuits
  hidden: false
  selected: false
  expanded: true
  critical: false
  subgroups:
    # ------------    
    - name: Simple
    # ------------
      description: For MS-Paint-like uses
      expanded: false
      packages:
        - name: drawing
          description: Simple image editor
        - name: kolourpaint
          description: Free graphics editor similar to MS Paint
        - name: mypaint
          description: Free software graphics application for digital painters
        - name: pinta 
          description: Drawing and editing program modeled after Paint.NET
    # ---------------------
    - name: Raster Graphics
    # ---------------------
      description: Edit images made of pixels. For Adobe-Photoshop-like uses
      expanded: false
      packages: 
        - name: krita
          description: Digital painting and illustration software
        - name: gimp
          description: Image editing suite with a large number of plugins
    # ---------------------
    - name: Vector Graphics
    # ---------------------
      description: Edit images made of geometric shapes. For Zoom-able Diagrams or ClipArt
      expanded: false
      packages: 
        - name: inkscape
          description: Vector graphics editor with SVG support, similar to Adobe Illustrator

# =================
- name: Videography
# =================
  description: Tools for Youtubers, Twitch streamers and their ilk
  hidden: false
  selected: false
  expanded: true
  critical: false
  packages: 
    - name: ffmpeg 
      description: Complete, cross-platform solution to record, convert and stream audio and video
  subgroups:
    # ------------
    - name: Webcam
    # ------------
      expanded: false
      packages:        
        - name: cheese
          description: Take photos and videos with your webcam, with fun graphical effects
        - name: kamoso
          description: Webcam recorder from the KDE community
        - name: pantheon-camera
          description: Camera app designed for elementary OS
        - name: motion
          description: Monitors video signals from many types of camera and can detect motion 
        - name: v4l-utils
          description: Configure webcam parameters like brightness and color       
    # ----------------------
    - name: Screen Recording
    # ----------------------
      expanded: false
      packages:
        - name: obs-studio
          description: Video recording and live streaming application
        - name: vokoscreen
          description: Simple screencast GUI tool using GStreamer
        - name: peek
          description: Simple screencast tool that produces GIF, APNG, WebM or MP4 animations
        - name: simplescreenrecorder
          description: Feature-rich screen recorder written in C++/Qt5 that supports X11 and OpenGL
        - name: menyoki
          description: Screen{shot,cast} and perform ImageOps on the command line
    # ----------------------
    - name: Video Conversion
    # ----------------------
      expanded: false
      packages: 
        - name: handbrake
          description: Simple yet powerful video transcoder ideal for batch mkv/x264 ripping. GTK version
        - name: ciano
          description: Simple multimedia file converter using FFmpeg and ImageMagick
        - name: transmageddon
          description: Simple python application for transcoding video into formats supported by GStreamer
    # -------------------
    - name: Video Editing
    # -------------------
      expanded: false
      packages:
        - name: shotcut
          description: Free, open source, cross-platform video editor
        - name: kdenlive
          description: Non-linear video editor designed for basic to semi-professional work
        - name: flowblade
          description: Multitrack non-linear video editor released under GPL3 license
        - name: openshot
          description: Non-linear video editor based on MLT framework.
        - name: pitivi
          description: Free video editor with a beautiful and intuitive user interface, a clean codebase and a fantastic community
        - name: avidemux-qt
          description: Free video editor designed for simple cutting, filtering and encoding tasks
        - name: vidcutter
          description: Fast lossless media cutter + joiner w/ frame-accurate SmartCut options powered by mpv, FFmpeg via a sleek Qt5 GUI

# =================
- name: Development
# =================
  description: Utilities for programming and application design
  hidden: false
  selected: false
  expanded: true
  critical: false
  packages: 
    - name: base-devel 
      description: A group of essential packages commonly used for development
  subgroups:
    # ----------------------
    - name: Editors and IDEs
    # ----------------------
      expanded: false
      subgroups:
        - name: VS Code
        # =============
          subgroups:
            - name: Code
            # ----------
              description: Open-source release built from the official code-oss repository
              selected: false
              packages:
                - code
                # - code-git
            - name: VSCodium
            # --------------
              description: A community-driven, freely-licensed binary distribution of VS Code
              packages:
                - vscodium-bin
                # - vscodium
                # - vscodium-git
            - name: Visual Studio Code
            # ------------------------
              description: Microsoft-branded binary release with telemetry
              packages:
                - visual-studio-code-bin
                # - visual-studio-code-insiders-bin
      packages:
        - name: atom
          description: A hackable text editor for the 21st Century
        - name: sublime-text-4
          description: The sophisticated text editor for code, markup and prose
    # -----------------------------------------
    - name: Scripting and Programming Languages
    # -----------------------------------------
      expanded: false
      subgroups:
        - name: Rust (Stable)
        # ===================
          packages:
            - rustup
          post-install: /bin/sh -c \"rustup toolchain install stable && rustup default stable\"
        - name: Rust (Beta)
        # ===================
          packages:
            - rustup
          post-install: /bin/sh -c \"rustup toolchain install beta && rustup default beta\"
        - name: Rust (Nightly)
        # ===================
          packages:
            - rustup
          post-install: /bin/sh -c \"rustup toolchain install nightly && rustup default nightly\"
        - name: Python 3
        # ===================
          subgroups:
            - name: Qt Bindings
            # -----------------
              subgroups:
                - name: PySide (Official)
                # :::::::::::::::::::::::
                  packages: 
                    - pyside2
                    - pyside2-tools
                    - pyside6
                    - pyside6-tools  
                - name: pyQt
                # :::::::::::::::::::::::
                  packages:
                    - python-pyqt5            
            - name: Gtk Bindings
            # -----------------
              packages: 
                - python-gobject
            - name: wxPython
            # -----------------
              packages: 
                - python-wxpython
          packages:
            - python
            - python-pip
            - python-pipenv
            - ipython
            - jupyterlab
            - jupyter-notebook
            - anaconda
    # ---------------------
    - name: Version Control
    # ---------------------
      description: Version Control Systems (VCS)
      expanded: false
      packages:
        - name: git
          description: The fast distributed version control system
        - name: subversion
          description: A Modern Concurrent Version Control System
        - name: gitkraken
          description: The intuitive, fast, and beautiful cross-platform Git client
        - name: git-cola
          description: The highly caffeinated Git GUI
        - name: smartgit
          description:  	Git client with Hg and SVN support
        - name: sublime-merge
          description: Meet a new Git Client, from the makers of Sublime Text

# ====================
- name: Administration
# ====================
  description: Tools for system administration
  hidden: false
  selected: false
  expanded: true
  critical: false
  subgroups: 
  # -------------
  - name: Storage
  # -------------
    expanded: true
    subgroups:
      - name: Filesystems
      # =================
        description: Support for various filesystems
        expanded: false
        subgroups: 
          - name: exFAT
          # -----------
            description: exFAT support
            packages: 
              - exfatprogs
              - exfat-utils
          - name: APFS
          # ----------
            description: APFS support
            packages: 
              - linux-apfs-rw-dkms-git
              - apfsprogs-git
          - name: Bcachefs
          # -------------
            description: Bcachefs support
            packages: 
              - linux-bcachefs-git
              - bcachefs-tools-git
          - name: ZFS
          # ---------
            description: ZFS support
            packages: 
              - zfs-linux
              - zfs-dkms
              - zfs-utils
        packages:           
            - name: dosfstools
              description: VFAT support
            - name: e2fsprogs
              description: ext3 and ext4 support
            - name: ntfs-3g
              description: NTFS support          
            - name: btrfs-progs
              description: BTRFS support
            - name: xfs-tools
              description: XFS support
            - name: reiserfsprogs
              description: ReiserFS support
            - name: f2fs-tools
              description: F2FS support
            - name: hfsprogs
              description: HFS and HFS+ support          
            - name: jfsutils
              description: JFS support
            - name: nilfs-utils
              description: NILFS2 support          
            - name: udftools
              description: UDF support
            - name: reiser4progs
              description: Reiser4 support
      - name: Partitioning
      # ==================
        expanded: false
        subgroups: 
          - name: Commandline
            packages:
              - fdisk
              - parted
              - cfdisk
              - sfdisk            
          - name: Graphical
            packages:
              - name: gparted
                description: GUI frontend for GNU parted
              - name: gnome-disk-utility
                description: Default partitioning utility in Gnome 3
              - name: partitionmanager
                description: Default partitioning utility in KDE Plasma
          

        

    